#!/bin/sh

#========>>>> some useful options (man zshoptions) <<<<========#
setopt autocd extendedglob nomatch menucomplete
setopt interactive_comments
stty stop undef		# Disable ctrl-s to freeze terminal.
zle_highlight=('paste:none')

# beeping is annoying
unsetopt BEEP

#========>>>> completions <<<<========#
autoload -Uz compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
# zstyle ':completion::complete:lsof:*' menu yes select
zmodload zsh/complist
compinit
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

#========>>>> Colors <<<<========#
autoload -Uz colors && colors

#========>>>> Useful Functions <<<<========#
source "$ZDOTDIR/functions"

#========>>>> Normal files to source <<<<========#
zsh_add_file "exports"
zsh_add_file "zshenv"
zsh_add_file "vim-mode"
zsh_add_file "aliases"
zsh_add_file "prompt"
zsh_add_file "keybinds"



#========>>>> Plugins <<<<========#
# For more plugins: https://github.com/unixorn/awesome-zsh-plugins
#to install plugins from github type "zplug_github"
#to install plugins from gitlab type "zplug_gitlab"
zplug_github "zsh-users/zsh-autosuggestions"
zplug_github "zsh-users/zsh-history-substring-search"
zplug_github "MichaelAquilina/zsh-auto-notify"
zplug_github "zsh-users/zsh-syntax-highlighting"

#------------------------------------------------------------------------------------#

#========>>>> completions <<<<========#
# More completions https://github.com/zsh-users/zsh-completions
#to install completions from github type "zcompletion_github"
#to install completions from gitlab type "zcompletion_gitlab"
zcompletion_github "esc/conda-zsh-completion" false


#========>>>> FZF <<<<========#
#source /usr/share/fzf/key-bindings.zsh
#source /usr/share/fzf/completion.zsh
# export FZF_DEFAULT_COMMAND='rg --hidden -l ""'

#========>>>> Edit line in vim with ctrl-e: <<<<========#
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

#========>>>> zsh-auto-notify plugin setting <<<<========#
export AUTO_NOTIFY_IGNORE=(
"nvim" "ranger" "lf" "less" "more" "man" "htop" "nano" "top" "git" "vim"
)


