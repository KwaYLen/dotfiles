# KwaYLen dotfiles


## Support

 hado ghir dotfiles dyali flinux setup diyali, makayen ta 7aja special fihom.

3azizi l9ari2 hada discord server diyali la bghiti chi mosa3ada [My Discord server](https://discord.gg/cMwYk7VSwu)!

  

## USE MY DOTFILES


``` git clone https://gitlab.com/KwaYLen/dotfiles ~/ ```
- fach dir clone lrepo dir copy lconfig dlapp/programe/etc li bghitit ou 7ato fnefs lblasa li ma7tot fiha fhad dotfile
- `exemple:` (dofiles/.config/kitty) 5asek diro f (~/.config/)

```` La kan chi update hna ou bghiti tinstalih dir (git pull) fdotfiles directory li dirtiliha clone ````

  

## Tools/Apps/Programs that i use
first am not using a desktop environment, ana kansta3mel window manager, 7it more productive, ou less bloated mn desktop environment

and am using : `Arch Btw`

- my X window manager: [awesome](https://awesomewm.org/)
- my wayland window manager (compositor): [hyprland](https://hyprland.org/)
- my terminal of choice is: [kitty](https://sw.kovidgoyal.net/kitty/)
- my shell of choice: [zsh](https://www.zsh.org/)
- my media player of choice: [mpv](https://mpv.io/)
- editor of choice is: [nvim](https://neovim.io/)
- gui editor of choice: [emacs](https://www.gnu.org/software/emacs/), some times i use [VScodium](https://vscodium.com/)
- to manage the dotfiles you are in now i use: [gnu stow](https://www.gnu.org/software/stow/)
- my browsers of choice are: [FireFox](https://www.mozilla.org/en-US/firefox/new/)/[thorium](https://thorium.rocks/)/[qutebrowser](https://qutebrowser.org/)(for vim like browser), and the [Tor Browser](https://www.torproject.org/) some times.
- my file manager of choice is: [dolphine](https://apps.kde.org/dolphin/), 3arf 5aso bzaf diyal dependencise walakin chaklo m9awed m3a chwiya dlblur.

## Name

- `Kdotfiles`

 
## Authors

ghir ana!!!

  

## License

had dot files te7t liecense GPL-3.0 y3ni la bghiti ta5od had les config ou teditihom bsmaytek (ila a5irih mn dakchi li kayes flicense), 5od ra7tek, la biti more info 9ra had l9wada [KWAYLES DOTFILES LIECENSE](https://gitlab.com/KwaYLen/dotfiles/-/blob/main/LICENSE?ref_type=heads), wa5a 3aref ta wa7ed ma ghadi y9ra 700 line dlktaba.
